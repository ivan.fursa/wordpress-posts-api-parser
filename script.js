let currentPage = 1
async function getData(site,post){
  if(post){
   await fetch(site +"/wp-json/wp/v2/posts/"+post)
  .then((response) => response.json())
  .then((out) => {
    let main = document.getElementById('content')
    main.innerHTML = ``
    main.innerHTML += ` <button onclick="getData('${site}')">All posts</button> `
    main.innerHTML += `<h1>${out.title.rendered}</h1>`
    main.innerHTML += `<div>${out.content.rendered}</div>`
  });
  }
  else{
    let main = document.getElementById('content')
    main.innerHTML = ``
    await fetch(site +"/wp-json/wp/v2/posts/?page="+currentPage)
    .then((response) => response.json())
    .then((posts) => {
      console.log(posts)
      for(let post of posts){
        let siteUrl = post.guid.rendered
        siteUrl = siteUrl.split('?')
        siteUrl = siteUrl[0]
        
        main.innerHTML += `
        <div class="post">
          <h3>${post.title.rendered}</h3>
          <button onclick="getData('${siteUrl}','${post.id}')">${post.id}</button>
        </div>
          <hr>`
      }
      });
  }
}

async function getDataList(page){
let main = document.getElementById('content')
main.innerHTML = ``
await fetch("https://udachnyj-enot.com.ua/wp-json/wp/v2/posts?page="+page)
.then((response) => response.json())
.then((posts) => {
  console.log(posts)
  for(let post of posts){
    let siteUrl = post.guid.rendered
    siteUrl = siteUrl.split('?')
    siteUrl = siteUrl[0]
    main.innerHTML += `<h2>${post.title.rendered}</h2>`
    main.innerHTML += `<button onclick="getData('${siteUrl}','${post.id}')">${post.id}</button>`
    main.innerHTML += `<hr>`
  }
  });
}
let site
let post

function parseInit() {
  site = document.getElementById("site").value 
  post = document.getElementById("post").value
  if(post)
  {
    getData(site,post)
  }
  else{
    getData(site)
  }
}


function nextPage(){
  currentPage += 1 
  getData(site)
}
function prevPage(){
  if(currentPage < 2)
  {
    return 
  }
  else{
    currentPage -= 1
    getData(site)
  }
}

async function searchPosts(siteSearch,query,queryPage){
  let main = document.getElementById('content')
  main.innerHTML = ``
  await fetch(siteSearch +"/wp-json/wp/v2/search?search="+query+"&per_page=10&page="+queryPage)
  .then((response) => response.json())
  .then((posts) => {
    console.log(posts)
    for(let post of posts){
      main.innerHTML += `
      <div class="post">
        <h3>${post.title}</h3>
        <button onclick="getData('${siteSearch}','${post.id}')">${post.id}</button>
      </div>
        <hr>`
    }
    });
}

function nextPageSearch(){
  
}

function searchInit() {
  queryPage = 1
  let siteSearch = document.getElementById("site-search").value 
  let query = document.getElementById("query").value
  if(siteSearch && query)
  {
    searchPosts(siteSearch,query)
  }
  else{
    alert('no results')
  }
}